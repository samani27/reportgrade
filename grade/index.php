<?php

require_once(dirname(__FILE__) . '/../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once(dirname(__FILE__) . '/report_form.php');
require_once(dirname(__FILE__) . '/lib.php');

require_login();
$context = context_system::instance();
require_capability('report/grade:view', $context);
$PAGE->set_context($context);
$mform = new report_grade_form();

admin_externalpage_setup('report_grade');
echo $OUTPUT->header();

echo html_writer::tag('h1',get_string('pluginname','report_grade'));
echo html_writer::empty_tag('br');

$mform->display();

$data = $mform->get_data();


if($data){
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	echo html_writer::empty_tag('br');
	
	$result = get_grade($data->course);
	
	if($result){
		$table = '';
		$table .= html_writer::tag("th", "#");
		$table .= html_writer::tag("th", "Course");
		$table .= html_writer::tag("th", "Username");
		$table .= html_writer::tag("th", "Final Grade");
		$table = html_writer::tag("tr", $table);
		$i = 0;
		foreach ($result as $key => $value) {
			$row = '';
			$row .= html_writer::tag("td", ++$i);
			$row .= html_writer::tag("td", $value->fullname);
			$row .= html_writer::tag("td", $value->username);
			$row .= html_writer::tag("td", $value->finalgrade);
			$row = html_writer::tag("tr", $row);
			$table .= $row;
			
			
		}

		echo html_writer::tag("table", $table, array("class" => "table table-bordered table-stripped table-hover"));
		
	} else {
		echo html_writer::tag('p',"Data not found");
	}

} 



echo $OUTPUT->footer();
