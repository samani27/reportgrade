<?php

$string['grade:view'] = 'View total grade per course report';
$string['pluginname'] = 'Grade Report';
$string['start'] = 'Start Date';
$string['end'] = 'End Date';
$string['timenotvalid'] = "Start date must be greater than End data";
$string['course'] = 'Course';