<?php

function get_grade($courseid)
{
	global $DB;

	$where = '';
	if($courseid != "0"){
		$where = 'and c.id = '.$courseid;
	}

	$sql = 'SELECT gg.id, u.username, gi.itemname, c.fullname, gg.finalgrade 
			FROM {grade_grades} gg
			JOIN {grade_items} gi on gg.itemid=gi.id
			JOIN {course} c on c.id=gi.courseid
			JOIN {user} u on u.id=gg.userid
			where gg.finalgrade is not null and itemname is null '.$where.' order by c.fullname, u.username';

	$result = $DB->get_records_sql($sql);

	if(empty($result)){
		return false;
	}

	return $result;

}